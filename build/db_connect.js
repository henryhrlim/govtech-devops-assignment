let mysql = require('mysql');
let connection = mysql.createConnection({
    host: 'mysql',
    user: 'root',
    password: 'password',
    database: 'sample'
});
connection.connect(function(err) {
  if (err) {
    return console.error('error: ' + err.message);
  }

  console.log('Connected to the MySQL server.');
});

connection.query("SELECT * FROM app", function (err, result, fields) {
    if (err) {
      return console.error('error: ' + err.message);
    };

    console.log(result);
});
