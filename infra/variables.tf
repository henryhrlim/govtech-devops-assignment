variable "ami" {
  type    = string
  default = "ami-0bd6906508e74f692"
}

variable "vpc_cidr" {
  type        = string
  default     = "10.0.0.0/16"
  description = "default vpc_cidr_block"
}

variable "pub1_cidr_block" {
  type    = string
  default = "10.0.1.0/24"
}

variable "pub2_cidr_block" {
  type    = string
  default = "10.0.2.0/24"
}
variable "pub3_cidr_block" {
  type    = string
  default = "10.0.3.0/24"
}

variable "prv1_cidr_block" {
  type    = string
  default = "10.0.101.0/24"
}

variable "prv2_cidr_block" {
  type    = string
  default = "10.0.102.0/24"
}

variable "prv3_cidr_block" {
  type    = string
  default = "10.0.103.0/24"
}
