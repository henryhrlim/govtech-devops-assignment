# Govtech DevOps Assignment

## Scenario 1: Terraform, AWS and CICD
### Further improvements to architecture
- To use AWS Route 53 for DNS support to use private domains, instead of using ELB domains for access
- Further improvement for this architecture for hosting any server application is to deploy out to multiple geolocations and available zones (e.g. us-west-1, ap-south-1 etc.) for better support on high-availability and disaster recovery. 
- Alternative architecture for hosting static website (this is the case for this assignment) is to use Route 53 -> CDN (e.g. Cloudfront) -> S3. This may give better manageability and potentially better cost savings for this use case. 

### Further improvements to setup
- To use configuration management tools for managing infrastruction configs (e.g. installing Nginx, setting up server initialization scripts etc.) such as Ansible or Chef
- To use secrets management tools such as AWS Secrets Manager or AWS KMS to manage keys and credentials during infra deployment or updates.

---
## Scenario 2: K8s
### Assumptions made
- Stateless application (increment-decrement-counter) does not use data from mysql (i.e. only made connection)
- Stateful application (mysql) are not replicated, running on single instance

### Build containter
1. Run `docker build -t increment-decrement-counter .`

### Deploy mysql server
1. Claim persistent volume by `kubectl apply -f mysql-pv.yml`
2. Deploy mysql server by `kubectl apply -f mysql-deploy.yml`
3. (Optional) Expose mysql server to host by `kubectl expose deployment mysql --type=LoadBalancer --name=mysql-service`

### Deploy application
1. Deploy application by `kubectl apply -f app-deploy.yml`
2. Expose application to host by `kubectl expose deployment increment-decrement-counter --type=LoadBalancer --name=increment-decrement-counter-service`
3. To verify deployment successfully connected to mysql server, check deployment logs via `kubectl logs -f deployment/increment-decrement-counter-deployment`. You should see log entry `Connected to the MySQL server.`, followed by some queried data rows in the DB. 
4. You can now access your application via `localhost` from your browser

### Install prometheus helm chart
1. Add prometheus helm repo by `helm repo add prometheus-community https://prometheus-community.github.io/helm-charts`
2. Create namespace for prometheus by `kubectl create namespace prometheus`
3. Install prometheus helm chart by `helm upgrade -i prometheus prometheus-community/prometheus --namespace prometheus`
4. Note down prometheus server access url (e.g. `prometheus-server.prometheus.svc.cluster.local`)

### Install grafana helm chart
1. Add grafana helm repo by `helm repo add grafana https://grafana.github.io/helm-charts`
2. Create namespace for grafana by `kubectl create namespace grafana`
3. Install grafana helm chart by ` helm upragde -i -f grafana.yml grafana grafana/grafana --namespace grafana`
4. You can now access your local instance of Grafana by `localhost:3000` from your browser
5. (Optional) Import your favourite dashboards such as Kubernetest Cluster monitor dashboard (via https://grafana.com/grafana/dashboards/6417) on Grafana UI


