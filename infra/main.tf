terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.15.1"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "ap-southeast-1"
}

data "aws_availability_zones" "available" {
  state = "available"
}

# Create VPC
resource "aws_vpc" "main" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true
}

# Create Public Subnet 1
resource "aws_subnet" "pub_sub_1" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.pub1_cidr_block
  availability_zone       = "ap-southeast-1a"
  map_public_ip_on_launch = true
}

# Create Public Subnet 2
resource "aws_subnet" "pub_sub_2" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.pub2_cidr_block
  availability_zone       = "ap-southeast-1b"
  map_public_ip_on_launch = true
}

# Create Public Subnet 3
resource "aws_subnet" "pub_sub_3" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.pub3_cidr_block
  availability_zone       = "ap-southeast-1c"
  map_public_ip_on_launch = true
}

# Create Private Subnet 1
resource "aws_subnet" "prv_sub_1" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.prv1_cidr_block
  availability_zone       = "ap-southeast-1a"
  map_public_ip_on_launch = false
}

# Create Private Subnet 2
resource "aws_subnet" "prv_sub_2" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.prv2_cidr_block
  availability_zone       = "ap-southeast-1b"
  map_public_ip_on_launch = false
}

# Create Private Subnet 3
resource "aws_subnet" "prv_sub_3" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.prv3_cidr_block
  availability_zone       = "ap-southeast-1c"
  map_public_ip_on_launch = false
}

# Create Internet Gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id
}

# Create Public Route Table
resource "aws_route_table" "pub_rt" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
}

# Create route table association of public subnet
resource "aws_route_table_association" "internet_for_pub1" {
  route_table_id = aws_route_table.pub_rt.id
  subnet_id      = aws_subnet.pub_sub_1.id
}
resource "aws_route_table_association" "internet_for_pub2" {
  route_table_id = aws_route_table.pub_rt.id
  subnet_id      = aws_subnet.pub_sub_2.id
}
resource "aws_route_table_association" "internet_for_pub3" {
  route_table_id = aws_route_table.pub_rt.id
  subnet_id      = aws_subnet.pub_sub_3.id
}

# Create EIP for NAT GW
resource "aws_eip" "eip_natgw" {
  count = "3"
}

# Create NAT gateway
resource "aws_nat_gateway" "natgateway1" {
  count         = "1"
  allocation_id = aws_eip.eip_natgw[count.index].id
  subnet_id     = aws_subnet.pub_sub_1.id
}
resource "aws_nat_gateway" "natgateway2" {
  count         = "1"
  allocation_id = aws_eip.eip_natgw[count.index + 1].id
  subnet_id     = aws_subnet.pub_sub_2.id
}
resource "aws_nat_gateway" "natgateway3" {
  count         = "1"
  allocation_id = aws_eip.eip_natgw[count.index + 2].id
  subnet_id     = aws_subnet.pub_sub_3.id
}


# Create private route table for prv sub
resource "aws_route_table" "prv_rt_1" {
  count  = "1"
  vpc_id = aws_vpc.main.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.natgateway1[count.index].id
  }
}
resource "aws_route_table" "prv_rt_2" {
  count  = "1"
  vpc_id = aws_vpc.main.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.natgateway2[count.index].id
  }
}
resource "aws_route_table" "prv_rt_3" {
  count  = "1"
  vpc_id = aws_vpc.main.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.natgateway3[count.index].id
  }
}

# Create route table association betn prv subnet & NAT GW
resource "aws_route_table_association" "prv_sub1_to_natgw1" {
  count          = "1"
  route_table_id = aws_route_table.prv_rt_1[count.index].id
  subnet_id      = aws_subnet.prv_sub_1.id
}
resource "aws_route_table_association" "prv_sub2_to_natgw2" {
  count          = "1"
  route_table_id = aws_route_table.prv_rt_2[count.index].id
  subnet_id      = aws_subnet.prv_sub_2.id
}
resource "aws_route_table_association" "prv_sub3_to_natgw3" {
  count          = "1"
  route_table_id = aws_route_table.prv_rt_3[count.index].id
  subnet_id      = aws_subnet.prv_sub_3.id
}

# Create security group for load balancer
resource "aws_security_group" "alb_sg" {
  name   = "alb_sg"
  vpc_id = aws_vpc.main.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    description = "HTTP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create security group for webserver
resource "aws_security_group" "webserver_sg" {
  name   = "webserver_sg"
  vpc_id = aws_vpc.main.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    description = "HTTP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# create alb
resource "aws_lb" "govtech-devops-entrytask" {
  name               = "govtech-devops-entrytask-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  subnets            = [aws_subnet.pub_sub_1.id, aws_subnet.pub_sub_2.id, aws_subnet.pub_sub_3.id]
}

# create alb listener
resource "aws_lb_listener" "govtech-devops-entrytask" {
  load_balancer_arn = aws_lb.govtech-devops-entrytask.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.govtech-devops-entrytask.arn
  }
}

# create target group
resource "aws_lb_target_group" "govtech-devops-entrytask" {
  name       = "govtech-devops-entrytask"
  depends_on = [aws_vpc.main]
  port       = 80
  protocol   = "HTTP"
  vpc_id     = aws_vpc.main.id
}

# create s3 bucket
resource "aws_s3_bucket" "govtech-devops-entrytask" {
  bucket = "govtech-devops-entrytask"
  acl    = "private"
}

# upload required files to bucket
resource "aws_s3_object" "govtech-devops-entrytask" {
  bucket = "govtech-devops-entrytask"
  key    = "index.html"
  source = "../src/index.html"
}

# create launch configuration
resource "aws_launch_configuration" "govtech-devops-entrytask" {
  name_prefix     = "govtech-devops-entrytask-"
  image_id        = var.ami
  instance_type   = "t2.micro"
  user_data       = file("user-data.sh")
  security_groups = [aws_security_group.webserver_sg.id]

  lifecycle {
    create_before_destroy = true
  }
}

# create autoscaling group
resource "aws_autoscaling_group" "govtech-devops-entrytask" {
  name                 = "govtech-devops-entrytask-autoscaling-group"
  min_size             = 1
  max_size             = 3
  desired_capacity     = 3
  depends_on           = [aws_lb.govtech-devops-entrytask]
  target_group_arns    = [aws_lb_target_group.govtech-devops-entrytask.arn]
  launch_configuration = aws_launch_configuration.govtech-devops-entrytask.name
  vpc_zone_identifier  = [aws_subnet.prv_sub_1.id, aws_subnet.prv_sub_2.id, aws_subnet.prv_sub_3.id]
}